package eu.telecomnancy.sensor;
import java.util.ArrayList;
import eu.telecomnancy.ui.*;

public class TemperatureSensorAdapter extends LegacyTemperatureSensor implements ISensor {
	

	private LegacyTemperatureSensor ts;
	public TemperatureSensorAdapter() {
		this.ts=new LegacyTemperatureSensor();
	}
	@Override
	public void on() {
		if (this.getStatus()==false) {
			ts.onOff();
		} 
	}

	@Override
	public void off() {
		if (this.getStatus()==true) {
			ts.onOff();
		} 
	}

	@Override
	//Acquiring new value form LegacyTemperatureSensor
	public void update() throws SensorNotActivatedException {
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return ts.getTemperature();
	}

}

package eu.telecomnancy.sensor;

import java.util.Random;

public class StateTemperatureSensor implements ISensor {
	AbstractState state;
    double value = 0;

    @Override
    public void on() {
        state=new StateOn();
        state.showState();
    }

    @Override
    public void off() {
        state=new StateOff();
        state.showState();
    }

    @Override
    public boolean getStatus() {
        if (state instanceof StateOn) {
        	return true;
        } else {
        	return false;
        }
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state instanceof StateOn) {
            value = (new Random()).nextDouble() * 100;
            soable.notifyObservers();
        	System.out.println("Je m'update");
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state instanceof StateOn)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
}

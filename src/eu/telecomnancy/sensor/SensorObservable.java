package eu.telecomnancy.sensor;

import java.util.ArrayList;

import eu.telecomnancy.ui.SensorObserver;

public class SensorObservable {
	
	ArrayList<SensorObserver> a ;
	public SensorObservable() {
		 a= new ArrayList<SensorObserver>();
	}
	
	public void addObserver(SensorObserver so) {
		a.add(so);
	}
	
	public void deleteObserver(SensorObserver so) {
		a.remove(a.indexOf(so));
	}
	
	public void notifyObservers() {
		int i;
		for (i=0;i<a.size();i++) {
			a.get(i).update();
		}
	}
	
}
package eu.telecomnancy.sensor;

import java.sql.Date;

public class Proxy implements ISensor {
	
	private ISensor sensor;
	private Date date;
	@Override
	public void on() {
		sensor.on();
		System.out.println("Date : " + date.toString() + " m�thode appel�e : proxy.on()" + " retour : --(void)"  );
	}

	@Override
	public void off() {
		sensor.off();
		System.out.println("Date : " + date.toString() + " m�thode appel�e : proxy.off()" + " retour : --(void)"  );
	}

	@Override
	public boolean getStatus() {
		System.out.println("Date : " + date.toString() + " m�thode appel�e : proxy.getStatus()" + " retour : " + sensor.getStatus() );
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		System.out.println("Date : " + date.toString() + " m�thode appel�e : proxy.update()" + " retour : --void");
		sensor.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		System.out.println("Date : " + date.toString() + " m�thode appel�e : proxy.getValue()" + " retour : " + sensor.getValue()  );
		return sensor.getValue();
	}
	
	public ISensor getSensor() {
		return sensor;
	}

	public void setSensor(ISensor sensor) {
		this.sensor = sensor;
	}

}

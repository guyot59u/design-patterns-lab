package eu.telecomnancy.sensor;
import java.util.ArrayList;
import java.util.Random;
import eu.telecomnancy.ui.*;

public class TemperatureSensor implements ISensor  {

	boolean state;
    double value = 0;

    @Override
    public void on() {
        state = true;
        System.out.println("Je m'allume");
    }

    @Override
    public void off() {
        state = false;
        System.out.println("Je m'�teint");
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state) {
            value = (new Random()).nextDouble() * 100;
            soable.notifyObservers();
        	System.out.println("Je m'update");
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}

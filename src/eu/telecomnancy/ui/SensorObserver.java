package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.ISensor;

public interface SensorObserver {
	
	public void update(); 
}

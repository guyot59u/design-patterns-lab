package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensorAdapter;
import eu.telecomnancy.ui.ConsoleUI;

public class AdapterApp {

	public static void main(String[] args) {
		ISensor sensor = new TemperatureSensorAdapter();
		new ConsoleUI(sensor);
	}

}
